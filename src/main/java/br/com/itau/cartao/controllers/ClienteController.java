package br.com.itau.cartao.controllers;

import br.com.itau.cartao.dtos.ClienteDTOEntradaPost;
import br.com.itau.cartao.models.Cliente;
import br.com.itau.cartao.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> postCliente(@RequestBody ClienteDTOEntradaPost clienteDTOEntradaPost) {
        Cliente cliente = new Cliente();
        cliente.setName(clienteDTOEntradaPost.getName());

        Cliente clienteObjeto = clienteService.salvarCliente(cliente);

        return ResponseEntity.status(201).body(clienteObjeto);
    }

    @GetMapping("/{id}")
    public Cliente getClienteById(@PathVariable(name = "id") Long id) {
        try {
            Cliente clienteObjeto = clienteService.consultarClientePorId(id);
            return clienteObjeto;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
