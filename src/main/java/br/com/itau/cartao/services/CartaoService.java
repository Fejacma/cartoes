package br.com.itau.cartao.services;

import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.Cliente;
import br.com.itau.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao) {
        Cliente cliente = cartao.getCliente();
        long idCliente = cliente.getId();

        Cliente clienteObjeto = clienteService.consultarClientePorId(idCliente);
        cartao.setCliente(clienteObjeto);

        Cartao cartaoObjeto = cartaoRepository.save(cartao);

        return cartaoObjeto;
    }

    public Cartao ativarCartao(String numero, Cartao cartao) {
        if (cartaoRepository.existsByNumero(numero)) {
            Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
            cartao.setId(cartaoOptional.get().getId());
            cartao.setNumero(numero);
            cartao.setCliente(cartaoOptional.get().getCliente());
            Cartao cartaoObjeto = salvarCartao(cartao);

            return cartaoObjeto;
        }

        throw new RuntimeException("Cartão não encontrado.");
    }

    public Cartao consultarPorNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if (cartaoOptional.isPresent()) {
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não encontrado.");
    }

}
