package br.com.itau.cartao.services;

import br.com.itau.cartao.models.Cartao;
import br.com.itau.cartao.models.Pagamento;
import br.com.itau.cartao.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento) {

        Cartao cartaoObjeto = cartaoService.consultarPorNumero(pagamento.getCartao().getNumero());
        boolean ativo = cartaoObjeto.isAtivo();

        pagamento.getCartao().setAtivo(ativo);

        if (ativo) {
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }

        throw new RuntimeException("Seu cartão não está ativo. Favor ativar o Cartão antes de realizar sua compra.");
    }

    public Iterable<Pagamento> consultarPagamentosPorCartaoId(long cartaoId) {
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(cartaoId);
        return pagamentos;
    }

}
