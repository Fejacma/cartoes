package br.com.itau.cartao.repositories;

import br.com.itau.cartao.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> { }
